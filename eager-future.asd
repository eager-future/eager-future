(asdf:defsystem :eager-future
  :serial t
  :components ((:file "package")
               (:file "future")
               (:file "threads")
               (:file "eager-future"))
  :depends-on (:bordeaux-threads))

(asdf:defsystem :eager-future.test
  :components ((:module :test
                        :serial t
                        :components ((:file "package")
                                     (:file "test"))))
  :depends-on (:eager-future :fiveam))
